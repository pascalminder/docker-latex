FROM ubuntu:20.10

MAINTAINER Pascal Minder <pascal.minder@trustus.ch>

ARG USER_NAME=latex
ARG USER_HOME=/home/latex
ARG USER_ID=1000
ARG USER_GECOS=LaTeX

ENV DEBIAN_FRONTEND noninteractive

RUN adduser \
  --home "$USER_HOME" \
  --uid $USER_ID \
  --gecos "$USER_GECOS" \
  --disabled-password \
  "$USER_NAME"

VOLUME ["/home/latex"]

RUN apt-get update -q \
   && apt-get install -qy --no-install-recommends \
      ca-certificates locales build-essential wget libfontconfig1 \
   && rm -rf /var/lib/apt/lists/*

# Add unicode support
RUN locale-gen en_US.UTF-8

ENV LANG 'en_US.UTF-8'
ENV LANGUAGE 'en_US:en'

# add dependencies
RUN set -x \
   && apt-get update \
   && apt-get install -y --no-install-recommends \
      wget \
      git \
      make \
      texlive-full \
      pandoc \
      pandoc-citeproc \
      python3-pygments \
      fig2dev \
      ghostscript \
   && apt-get clean autoclean \
   && apt-get autoremove -y \
   && rm -rf /var/lib/apt/lists/*

RUN tlmgr init-usertree && tlmgr update --self && tlmgr update --all
