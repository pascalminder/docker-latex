# LaTex Docker image

This image is intended to be used with the CI process of GitLab.

An example for the `.gitlab-ci.yml` file:
```
compile_pdf:
  image: pascalminder/latex
  script:
    - latexmk -r .latexmkrc -pdf 'work.tex' -jobname=work
  artifacts:
    paths:
      - ./build/work.pdf
```
This would use the `work.tex` as main latexfile and build it with the jobname `work`.

Additionaly the project need a `.latexmkrc ` file:
```
$latex = 'latex  %O  --shell-escape %S';
$pdflatex = 'lualatex  %O  --shell-escape %S';
$out_dir = 'build';
```
The `$out_dir` specifies where the built pdf file should be saved to.

Example to set up the docker container on a host:
```
docker run \
    --user="$(id -u):$(id -g)" 
    --net=none 
    -v "/Users/pascal/Downloads/latex:/home/latex"
    pascalminder/latex
```

Building the container:
```
docker build --no-cache -t pascalminder/latex:latest .
docker tag pascalminder/latex pascalminder/latex:latest
docker push pascalminder/latex:latest 
```